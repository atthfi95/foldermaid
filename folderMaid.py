from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import os, time

# Debug flag
debug = True

# TODO: parse these values from TOML file settings.conf, see example .conf

sleep_interval = 10                                    # Sleep interval
username = os.path.split(os.path.expanduser('~'))[-1]   # Define username automatically
monitor_folder = "/home/" + username + "/Downloads/"    # Folder to track
# [ file_extension, destination_folder ]
file_preferences = [
    [".pdf", "/Documents/"],
    [".docx", "/Documents/"],
    [".odt", "/Documents/"],
    [".sql", "/Documents/sql/"],
    [".txt", "/Documents/txt/"],
    [".crypt", "/Documents/crypted/"],
    [".zip", "/Documents/archived/"],
    [".rar", "/Documents/archived/"],
    [".7z", "/Documents/archived/"],
    [".tar", "/Documents/archived/"],
    [".xz", "/Documents/archived/"],
    [".iso", "/Documents/archived/img/"],
    [".img", "/Documents/archived/img/"],
    [".dmg", "/Documents/archived/img/"],
    [".vdi", "/Documents/archived/img/"],
    [".mp3", "/Music/"],
    [".aac", "/Music/"],
    [".ogg", "/Music/"],
    [".flac", "/Music/"],
    [".jpg", "/Pictures/"],
    [".jpeg", "/Pictures/"],
    [".png", "/Pictures/"],
    [".gif", "/Pictures/gifs/"],
    [".raw", "/Pictures/raw/"],
    [".svg", "/Pictures/svg/"],
    [".mp4", "/Videos/"],
    [".mpeg4", "/Videos/"],
    [".avi", "/Videos/"],
    [".mkv", "/Videos/"],
    [".go", "/go/src/local/"],
    [".java", "/Builds/src/java/"],
    [".class", "/Builds/src/java/"],
    [".bmx", "/Builds/src/blitzmax/"],
    [".c", "/Builds/src/ccpp/"],
    [".cpp", "/Builds/src/ccpp/"],
    [".h", "/Builds/src/ccpp/"],
    [".py", "/Builds/src/python/"],
    [".ipynb", "/Builds/src/python/"],
    [".sh", "/Builds/src/shellScripts/"],
    [".exe", "/Builds/bin/win_bin/"],
    [".jar", "/Builds/bin/jar/"],
    [".AppImage", "/Builds/bin/AppImages/"],
    [".unitypackage", "/Builds/bin/UnityPackages/"]
]

def bprint(msg):
    if debug: print(msg)

class Handler(FileSystemEventHandler):
    i = 1
    def on_modified(self, event):
        for file_extension, destination_folder in file_preferences:
            if not destination_folder.startswith("/home/"+username):
                destination_folder = "/home/" + username + destination_folder
            if not destination_folder.endswith("/"):
                destination_folder = destination_folder + "/"
            #bprint("Searching for " + file_extension + " files")
            new_name = "new_file_" + str(self.i) + file_extension
            for fname in os.listdir(monitor_folder):
                if fname.lower().endswith(file_extension.lower()):
                    file_exists = os.path.isfile(destination_folder + new_name)
                    while file_exists:
                        self.i += 1
                        new_name = "new_file_" + str(self.i) + file_extension
                        file_exists = os.path.isfile(destination_folder + "/" + new_name)
                    src = monitor_folder + fname
                    os.rename(src, destination_folder + fname)
                    bprint("Moving " + src + " to " + destination_folder)

event_handler = Handler()
observer = Observer()
observer.schedule(event_handler, monitor_folder, recursive=True)
observer.start()

try:
    while True:
        time.sleep(sleep_interval)
except KeyboardInterrupt:
    observer.stop()
    bprint("\nObserver stopped\n")
    os._exit
observer.join()